#ifndef __PARTICLES_CLASS__
#define __PARTICLES_CLASS__
// STD Stuff
#include <cstddef>
#include <random>
#include <cmath>
#include <vector>

#include "vector_math.h"

struct Color{
  float r;
  float g;
  float b;
  float a;
};

struct Particle{
  vec2 pos;
  vec2 vel;
  Color col;
};

struct ClusterData{
  vec2* pos;
  size_t c_id;
  float min_dist;
  std::vector<float> c_dist;
};

class Particles
{
public:
  Particles();
  ~Particles();

  void* get_storage();
  size_t get_storage_size();
  size_t get_particle_size();
  size_t get_n_particles();

  size_t n_clusters;
  std::vector<Color> cluster_colors;
  std::vector<vec2> cluster_speed;
  std::vector<ClusterData> m_c_storage;
  std::vector<vec2> cluster;
  std::vector<size_t> cluster_elements;
  void clustering(float dt);
  void clustering_dbscan(float dt);

  std::vector<Particle> m_storage;

  void generate_particles(size_t number);

  void check_bounds(Particle& e);
  void move(float dt);
  void repulsion(float dt);
  void limit_speed(Particle& e);

  void change_resolution(float w, float h);

private:
  float w_limit, h_limit;
};

#endif
