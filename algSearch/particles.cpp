#include "particles.h"

Particles::Particles(){}

Particles::~Particles(){
}

void Particles::generate_particles(size_t number){
  m_storage.resize(number);

  w_limit = 1;
  h_limit = 1;
  // All Particle Properties will be initilized to a random value between -1 and 1
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<float> dis(-1.0, 1.0);
  std::uniform_real_distribution<float> dis_c(0.0, 0.6);

  for(Particle& e:m_storage){
    e.pos.x = dis(gen);
    e.pos.y = dis(gen);
    e.col.r = dis_c(gen);
    e.col.g = dis_c(gen);
    e.col.b = dis_c(gen);
    e.col.a = 1;
  }
}

void* Particles::get_storage(){
  if(!m_storage.empty()){
    return (void*)&m_storage[0];
  }
  return nullptr;
}

size_t Particles::get_storage_size(){
  return m_storage.size() * sizeof(Particle);
}

size_t Particles::get_particle_size(){
  return sizeof(Particle);
}

size_t Particles::get_n_particles(){
  return m_storage.size();
}

void Particles::change_resolution(float w, float h){
  w_limit = w;
  h_limit = h;
}

void Particles::change_color(float bright){
  // All Particle Properties will be initilized to a random value between -1 and 1
  std::random_device rd;
  std::mt19937 gen(rd());

  if(bright > 1 || bright < 0){
    bright = 1;
  }
  
  std::uniform_real_distribution<float> dis_c(0.0, bright);

  for(Particle& e:m_storage){
    e.col.r = dis_c(gen);
    e.col.g = dis_c(gen);
    e.col.b = dis_c(gen);
    e.col.a = 1;
  }
}
