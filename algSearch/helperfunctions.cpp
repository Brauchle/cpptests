#include "helperfunctions.h"

void print_manual(){
  std::cout << "Parameters: <Method> <Constellation Size> <Population Size>\n";
  std::cout << "Methods:\n";
  std::cout << "(0) Random Search\n";
  std::cout << "(1) Simulated Annealing\n";
  std::cout << "(2) Genetic Algorithm\n";
  std::cout << "(3) Particle Swarm\n\n";


  std::cout << "p - Start / Pause \n";
  std::cout << "c - Color Change \n";
  std::cout << "r - Reset \n";
  std::cout << "Space - Show Ratings \n";
  std::cout << "Enter - Enter / Leave Fullscreen \n";

  std::cout << "-----------------------------------------------------------\n\n";
}
