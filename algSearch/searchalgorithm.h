#ifndef __SEARCHALGORITHM_CLASS__
#define __SEARCHALGORITHM_CLASS__
// STD Stuff
#include <iostream>
#include <complex>
#include <cstddef>
#include <random>
#include <cmath>
#include <vector>
#include <algorithm>


class Searchalgorithm
{
public:
  Searchalgorithm();
  ~Searchalgorithm();

  enum Algorithms{ Random, SimulatedAnnealing, Genetic, ParticleSwarm };

  struct Constellation
  {
    float rating;
    float min_dist;
    float mean_dist;
    std::vector< std::complex<float> > cpoints;
    std::vector< std::complex<float> > bpoints;
    std::vector< std::complex<float> > spoints;
  };

  void init(Algorithms alg, size_t pop_size, size_t constellation_size);

  void reset();

  float get_x(size_t con, size_t pnt);
  float get_y(size_t con, size_t pnt);

  float get_x_best(size_t pnt);
  float get_y_best(size_t pnt);


  void update();

  void up_random();
  void up_genetic();
  void up_simulated_annealing();
  void up_particle_swarm();

  void ps_aplly_speed_limit(Constellation& e);

  void rate_constellations();
  void rate_single_constellation(Constellation& e);

  void cout_rating();

private:
  Algorithms used_algorithm;

  Constellation best;
  std::vector< Constellation > constellations;

  // rate_single_constellation

  // Parameters
  size_t alg_constellation_size;
  size_t alg_population_size;

  // P- Gen
  size_t ga_n_survivors;
  size_t ga_n_dead;
  float ga_mut_chance;
  float ga_mut_strength;

  // P - sa
  float sa_count;
  float sa_speed;
  float sa_prop_drop;
  float sa_temp;
  float sa_step_size;

  // ParticleSwarm
  float ps_speed;
  float ps_inertia;
  float ps_speed_l;
  float ps_speed_g;

  float ps_speed_ll;
  float ps_speed_ul;

};

#endif
