#version 330 core
layout (points) in;
layout (points, max_vertices = 5) out;

out vec4 vertexColor;

void main() {
    vertexColor = vec4(1.0, 1.0, 1.0, 1.0);
    gl_Position = gl_in[0].gl_Position;
    EmitVertex();

    vertexColor = vec4(0.0, 1.0, 1.0, 0.5);
    gl_Position = gl_in[0].gl_Position + vec4(-0.2, 0.0, 0.0, 0.0);
    EmitVertex();

    gl_Position = gl_in[0].gl_Position + vec4(0.0, -0.2, 0.0, 0.0);
    EmitVertex();

    gl_Position = gl_in[0].gl_Position + vec4(0.2, 0.0, 0.0, 0.0);
    EmitVertex();

    gl_Position = gl_in[0].gl_Position + vec4(0.0, 0.2, 0.0, 0.0);
    EmitVertex();

    EndPrimitive();
}
