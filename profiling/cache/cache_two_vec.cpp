#include <iostream>
#include <chrono>
#include <cstdlib>
#include <vector>
#include <complex>

#include <random>

float average_abs(std::vector<std::complex<float>>& buffer1, std::vector<std::complex<float>>& buffer2) {
  float res = 0;

  for (std::complex<float>& e : buffer1){
    res += std::abs(e);
  }

  for (std::complex<float>& e : buffer2){
    res += std::abs(e);
  }

  res = res / (buffer2.size()+buffer1.size());
  return res;
}

int main(int argc, char const *argv[]) {
  int v_size = 6000000;
  const int repetitions = 100;

  std::random_device rd{};
  std::mt19937 gen{rd()};
  std::normal_distribution<> d{0,2};

  std::chrono::high_resolution_clock::time_point t1;
  std::chrono::high_resolution_clock::time_point t2;
  std::chrono::duration<double> ms_time;

  std::vector<std::complex<float>> b1;
  std::vector<std::complex<float>> b2;
  float res_abs;

  v_size = 250000/sizeof(std::complex<float>);
  std::cout << 2*v_size/1000000.0 * sizeof(std::complex<float>) << " MB Data" << std::endl;
  b1.resize(v_size);
  b2.resize(v_size);
  for (size_t i = 0; i < v_size; i++) {
    b1[i] = std::complex<float>(d(gen),d(gen));
    b2[i] = std::complex<float>(d(gen),d(gen));
  }

  t1 = std::chrono::high_resolution_clock::now();
  for (size_t i = 0; i < repetitions; i++) {
    res_abs = average_abs(b1, b2);
  }
  t2 = std::chrono::high_resolution_clock::now();

  ms_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1);
  //ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
	std::cout << "time  : " << ms_time.count() << " " << res_abs << std::endl;




  v_size = 500000/sizeof(std::complex<float>);
  std::cout << 2*v_size/1000000.0 * sizeof(std::complex<float>) << " MB Data" << std::endl;
  b1.resize(v_size);
  b2.resize(v_size);
  for (size_t i = 0; i < v_size; i++) {
    b1[i] = std::complex<float>(d(gen),d(gen));
    b2[i] = std::complex<float>(d(gen),d(gen));
  }

  t1 = std::chrono::high_resolution_clock::now();
  for (size_t i = 0; i < repetitions; i++) {
    res_abs = average_abs(b1, b2);
  }
  t2 = std::chrono::high_resolution_clock::now();

  ms_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1);
  //ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
	std::cout << "time  : " << ms_time.count() << " " << res_abs << std::endl;




  v_size = 1000000/sizeof(std::complex<float>);
  std::cout << 2*v_size/1000000.0 * sizeof(std::complex<float>) << " MB Data" << std::endl;
  b1.resize(v_size);
  b2.resize(v_size);
  for (size_t i = 0; i < v_size; i++) {
    b1[i] = std::complex<float>(d(gen),d(gen));
    b2[i] = std::complex<float>(d(gen),d(gen));
  }

  t1 = std::chrono::high_resolution_clock::now();
  for (size_t i = 0; i < repetitions; i++) {
    res_abs = average_abs(b1, b2);
  }
  t2 = std::chrono::high_resolution_clock::now();

  ms_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1);
  //ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
	std::cout << "time  : " << ms_time.count() << " " << res_abs << std::endl;




  v_size = 2000000/sizeof(std::complex<float>);
  std::cout << 2*v_size/1000000.0 * sizeof(std::complex<float>) << " MB Data" << std::endl;
  b1.resize(v_size);
  b2.resize(v_size);
  for (size_t i = 0; i < v_size; i++) {
    b1[i] = std::complex<float>(d(gen),d(gen));
    b2[i] = std::complex<float>(d(gen),d(gen));
  }

  t1 = std::chrono::high_resolution_clock::now();
  for (size_t i = 0; i < repetitions; i++) {
    res_abs = average_abs(b1, b2);
  }
  t2 = std::chrono::high_resolution_clock::now();

  ms_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1);
  //ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
	std::cout << "time  : " << ms_time.count() << " " << res_abs << std::endl;




  v_size = 4000000/sizeof(std::complex<float>);
  std::cout << 2*v_size/1000000.0 * sizeof(std::complex<float>) << " MB Data" << std::endl;
  b1.resize(v_size);
  b2.resize(v_size);
  for (size_t i = 0; i < v_size; i++) {
    b1[i] = std::complex<float>(d(gen),d(gen));
    b2[i] = std::complex<float>(d(gen),d(gen));
  }

  t1 = std::chrono::high_resolution_clock::now();
  for (size_t i = 0; i < repetitions; i++) {
    res_abs = average_abs(b1, b2);
  }
  t2 = std::chrono::high_resolution_clock::now();

  ms_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1);
  //ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
	std::cout << "time  : " << ms_time.count() << " " << res_abs << std::endl;




  v_size = 8000000/sizeof(std::complex<float>);
  std::cout << 2*v_size/1000000.0 * sizeof(std::complex<float>) << " MB Data" << std::endl;
  b1.resize(v_size);
  b2.resize(v_size);
  for (size_t i = 0; i < v_size; i++) {
    b1[i] = std::complex<float>(d(gen),d(gen));
    b2[i] = std::complex<float>(d(gen),d(gen));
  }

  t1 = std::chrono::high_resolution_clock::now();
  for (size_t i = 0; i < repetitions; i++) {
    res_abs = average_abs(b1, b2);
  }
  t2 = std::chrono::high_resolution_clock::now();

  ms_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1);
  //ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
	std::cout << "time  : " << ms_time.count() << " " << res_abs << std::endl;




  v_size = 16000000/sizeof(std::complex<float>);
  std::cout << 2*v_size/1000000.0 * sizeof(std::complex<float>) << " MB Data" << std::endl;
  b1.resize(v_size);
  b2.resize(v_size);
  for (size_t i = 0; i < v_size; i++) {
    b1[i] = std::complex<float>(d(gen),d(gen));
    b2[i] = std::complex<float>(d(gen),d(gen));
  }

  t1 = std::chrono::high_resolution_clock::now();
  for (size_t i = 0; i < repetitions; i++) {
    res_abs = average_abs(b1, b2);
  }
  t2 = std::chrono::high_resolution_clock::now();

  ms_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1);
  //ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
	std::cout << "time  : " << ms_time.count() << " " << res_abs << std::endl;




  v_size = 32000000/sizeof(std::complex<float>);
  std::cout << 2*v_size/1000000.0 * sizeof(std::complex<float>) << " MB Data" << std::endl;
  b1.resize(v_size);
  b2.resize(v_size);
  for (size_t i = 0; i < v_size; i++) {
    b1[i] = std::complex<float>(d(gen),d(gen));
    b2[i] = std::complex<float>(d(gen),d(gen));
  }

  t1 = std::chrono::high_resolution_clock::now();
  for (size_t i = 0; i < repetitions; i++) {
    res_abs = average_abs(b1, b2);
  }
  t2 = std::chrono::high_resolution_clock::now();

  ms_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1);
  //ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
	std::cout << "time  : " << ms_time.count() << " " << res_abs << std::endl;


  return 0;
}
