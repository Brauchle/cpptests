#include <iostream>
#include <chrono>
#include <cstdlib>

void multiplyAdd(float *buffer1, float* buffer2, float factor, size_t v_size){
  for (size_t i = 0; i < v_size; i++) {
    buffer1[i] += buffer2[i] * factor;
  }
}

int main(int argc, char const *argv[]) {
  int v_size = 1<<20;
  const int repetitions = 1000;

  std::chrono::high_resolution_clock::time_point t1;
  std::chrono::high_resolution_clock::time_point t2;

  std::chrono::duration<double> ms_time;

  //float* b1 = new float[v_size];
  //float* b2 = new float[v_size];

  float *b1 =  (float *) aligned_alloc(16, 16*v_size* sizeof *b1);
  float *b2 =  (float *) aligned_alloc(16, 16*v_size* sizeof *b2);



  v_size = 16 * v_size;
  std::cout << v_size << " Multiplications" << std::endl;
  std::cout << b1 << std::endl;
  std::cout << b2 << std::endl;

  for (size_t i = 0; i < v_size; i++) {
    b2[i] = i/v_size;
  }

  std::cout << "Aligned?: " << std::endl;
  t1 = std::chrono::high_resolution_clock::now();
  for (size_t i = 0; i < repetitions; i++) {
    multiplyAdd(b1, b2, 0.001f, v_size-2);
  }
  t2 = std::chrono::high_resolution_clock::now();

  ms_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1);
  //ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
	std::cout << "time  : " << ms_time.count() << std::endl;


  for (size_t i = 0; i < v_size; i++) {
    b2[i] = i/v_size;
  }

  std::cout << "Not Aligned?: " << std::endl;
  t1 = std::chrono::high_resolution_clock::now();
  for (size_t i = 0; i < repetitions; i++) {
    multiplyAdd(b1+1, b2+2, 0.001f, v_size-2);
  }

  //multiplyAdd(b1+1, b2+2, 0.001f, v_size-2);
  t2 = std::chrono::high_resolution_clock::now();

  ms_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1);
  //ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1);
	std::cout << "time  : " << ms_time.count() << std::endl;


  std::free(b1);
  std::free(b2);
  return 0;
}
