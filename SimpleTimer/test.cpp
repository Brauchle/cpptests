#include "simpletimer.h"

int main() {
  timer::tik();
  timer::tok();
  timer::tok<std::chrono::milliseconds>();
  timer::tok();
  return 0;
}
