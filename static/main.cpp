#include <iostream>

class TestClass{
public:
  TestClass(){
    count++;
  }
  ~TestClass(){
    count--;
  }

  static int count;

  void get_count(){
    std::cout << count << "\n";
  }
};

int TestClass::count = 0;

int main(int argc, char const *argv[]) {
  /* code */

  TestClass a;
  a.get_count();
  {
    TestClass b, c, d;
    b.get_count();
    c.get_count();
    d.get_count();
    a.get_count();
  }
  a.get_count();

  return 0;
}
