#include <vector>
#include <iostream>
#include <algorithm>


using bitlist = std::vector< unsigned int >;


std::ostream& operator<<(std::ostream& os, const bitlist bl)
{
    int pot = 0;
    bool mid = false;
    for(const unsigned int& e : bl){
      if( e == 1){
        if( mid ){
          os << "+ x^" << pot << " ";
        } else {
          os << "x^" << pot << " ";
          mid = true;
        }
      }
      pot++;
    }
    return os;
}

int deg(bitlist &a){
  int deg = -1;
  if(a.size() > 0){
    for (size_t i = 1; i < a.size(); i++) {
      if(a[i] == 1) {
        deg = i;
      }
    }
  }

  return deg;
}

void addbl(bitlist &a, const bitlist &b){
// Polynom Division
  if(b.size() > a.size()){
    a.reserve(b.size());
  }
  for (size_t i = 0; i < b.size(); i++) {
    if( i < a.size() ){
      a[i] = (a[i] + b[i])%2;
    } else {
      a.push_back(b[i]);
    }
  }
}

bitlist genbl(int cf){
  bitlist out;
  out.resize(cf + 1);
  out[cf] = 1;
  return out;
}

// bitlist operator*(bitlist a, bitlist b){
// // Remainder of Polynom Division
//
// }

bitlist shiftbl(bitlist &in, bitlist in2){
  int s = deg(in2);

  if(s > 0){
    bitlist res;
    int new_size;

    if( deg(in) > 0){
      new_size = deg(in) + s + 1;
    } else {
      new_size = s + 1;
    }

    res.resize(new_size);

    for (size_t i = new_size-1; i >= s ; --i) {
      res[i] = in[i-s];
    }

    return res;
  } else {
    return in;
  }
}

bitlist operator/(bitlist &a, bitlist &b){
// Polynom Division
  bitlist q;
  bitlist r = a;
  int d = deg(b);

  while( deg(r) >= d){
    bitlist s = genbl( deg(r) - d );
    addbl(q,s);
    addbl(r, shiftbl(b, s) );
  }

  return q;
}

bitlist operator%(bitlist a, bitlist b){
// Remainder of Polynom Division
  std::cout << " % # " << std::endl;
  bitlist q;
  bitlist r = a;
  int d = deg(b);

  while( deg(r) >= d){
    bitlist s = genbl( deg(r) - d );
    addbl(q,s);
    addbl(r, shiftbl(b, s) );
  }

  return r;
}



bitlist gcd(bitlist a, bitlist b){
  bitlist r_last;
  bitlist r_quot;
  bitlist r_this = a;
  bitlist r_next = b;

  while( !(deg(r_next) == -1) ){
    r_last = std::move(r_this);
    r_this = std::move(r_next);

    r_next = r_last%r_this;

    r_quot = r_last/r_this;

    std::cout << "r_last(" << deg(r_last) << ")" << r_last.size() << ":" << r_last << std::endl;
    std::cout << "r_this(" << deg(r_this) << ")" << r_this.size() << ":" << r_this << std::endl;
    std::cout << "r_next(" << deg(r_next) << ")" << r_next.size() << ":" << r_next << std::endl;
    std::cout << "r_quot(" << deg(r_quot) << ")" << r_quot.size() << ":" << r_quot << std::endl;
    std::cin.ignore();
  }

  return r_this;
}

int main(){
  bitlist a{0,0,1,1,0,0,1,1};
  bitlist b{0,1,1,0,0,1};

  std::cout << "a(" << deg(a) << "):" << a << std::endl;
  std::cout << "b(" << deg(b) << "):" << b << std::endl;
  std::cout << std::endl;
  //
  // std::cout << "Addition" << std::endl;
  // addbl(a, b);
  // std::cout << "a(" << deg(a) << "):" << a << std::endl;
  // std::cout << "b(" << deg(b) << "):" << b << std::endl;
  // std::cout << std::endl;
  //
  // std::cout << "Multiplication" << std::endl;
  // a = shiftbl(a, b);
  // std::cout << "a(" << deg(a) << "):" << a << std::endl;
  // std::cout << "b(" << deg(b) << "):" << b << std::endl;
  // std::cout << std::endl;

  std::cout << "GCD" << std::endl;
  bitlist c = gcd(a, b);
  std::cout << "a(" << deg(a) << "):" << a << std::endl;
  std::cout << "b(" << deg(b) << "):" << b << std::endl;
  std::cout << "c(" << deg(c) << "):" << c << std::endl;
  std::cout << std::endl;

  return 0;
}
