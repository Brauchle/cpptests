#include "data_gen.h"

void gen_data(double *x, double *y, int p_size){
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    //std::uniform_real_distribution<double> dis(0.0, 1.0);
    std::normal_distribution<double> dis(0,1);

    #pragma omp parallel for
    for (int i=0;i<p_size;i++) {
        x[i] = dis(gen);
        y[i] = dis(gen);
    }
}
