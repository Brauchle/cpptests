#include <iostream>
#include <fstream>

#include "sdlwrapper.h"
#include "genalg.h"
#include "timer.h"
#include "genalg_render.h"

#include <chrono>


namespace param{
  const int rounds = 100000;
  const float tim_perc = 0.005; // Prozent für Ausgabe in console Fortschritt

  const int population_size = 16; // For high dimensions 0.5 d otherwise ~d or max 10 d, high populations can be unhelpful
  const int survivors = 8;
  const int reproduce_n = population_size - survivors;
  const int symbols = 16;
  const float mutation_chance = 0.5; // to high prevents converegence
  // const float snr = 1;
}

int main(int, char**){
	SDLWrapper sdl(600, 600);
  // Initialization
  Population pop(param::population_size);
  init(pop, param::symbols);

  // Output File
  std::string filename = "out.dat";
  std::ofstream out(filename, std::ios::binary);

  // Store Parameters
  float tmp = param::rounds;
  out.write(reinterpret_cast<char *>(&tmp), sizeof tmp);
  tmp = param::population_size;
  out.write(reinterpret_cast<char *>(&tmp), sizeof tmp);
  tmp = param::symbols;
  out.write(reinterpret_cast<char *>(&tmp), sizeof tmp);

  timer::tik();
  int limit = param::rounds * param::tim_perc;
  int mult = 1;
  for (size_t i = 0; i < param::rounds; i++) {
    // Survive / Check Fitness / Kill half of the poluation
    selection(pop);

    // Mate / Mix genes
    reproduce(pop, param::reproduce_n);

    // Mutate
    mutate(pop, param::reproduce_n, param::mutation_chance);

		// Render
		// render_generation(pop, sdl);

    if( (i > (limit * mult))  || (i == 0) ){
    		// Render
    		render_generation(pop, sdl);
      std::cerr << "# Round " << limit * mult
                << " Fitness: " << pop[param::population_size - 1].fitness << std::endl;

      timer::tok(param::tim_perc * mult);

      mult++;
    }
  }
  std::cerr << std::endl <<  "#FINISHED" << std::endl;
  timer::tok();

  store_generation(out, pop);
  std::cerr << "1st: " << pop[param::population_size - 1].fitness << std::endl;
  for (size_t i = 0; i < param::symbols; i++) {
    std::cerr << "  " << pop[param::population_size - 1].genes[i]
              << " " << abs(pop[param::population_size - 1].genes[i]) << std::endl;
  }

  std::cerr << "Python: " << std::endl;
  std::cerr << "x=(";
  for (size_t i = 0; i < param::symbols; i++) {
    std::cerr << std::real(pop[param::population_size - 1].genes[i]);
    if(i!=param::symbols-1){std::cerr << ", ";}
    if(i%5 == 4){
      std::cerr << std::endl << "   ";
    }
  }
  std::cerr << ")" << std::endl;
  std::cerr << "y=(";
  for (size_t i = 0; i < param::symbols; i++) {
    std::cerr << std::imag(pop[param::population_size - 1].genes[i]);
    if(i!=param::symbols-1){std::cerr << ", ";}
    if(i%5 == 4){
      std::cerr << std::endl << "   ";
    }
  }
  std::cerr << ")" << std::endl;

  //SDL_Delay(4000);
	return 0;
}
