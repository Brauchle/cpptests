#ifndef __GEN_ALG_RENDER_FUNCTIONS__
#define __GEN_ALG_RENDER_FUNCTIONS__

#include "genalg.h"
#include "sdlwrapper.h"

void render_generation(Population& pop, SDLWrapper& sdl);

#endif
