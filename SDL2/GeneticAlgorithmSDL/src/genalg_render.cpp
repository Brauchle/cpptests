#include "genalg_render.h"

float proj( float in, int max_v ) {
  return (max_v/2.0) * (1.0 + in);
}

void render_generation(Population& pop, SDLWrapper& sdl){
  SDL_Renderer *renderer = sdl.ren();
  SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
  SDL_RenderClear(renderer);

  for (Individuum& e:pop) {
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
    for(cfloat& cf:e.genes){
      int px = std::round( proj(cf.real(), sdl.w()) );
      int py = std::round( proj(cf.imag(), sdl.h()) );
      SDL_RenderDrawPoint(renderer, px, py);
    }
  }

  // SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
  // SDL_RenderDrawLine(renderer, 320, 200, 300, 240);
  // SDL_RenderDrawLine(renderer, 300, 240, 340, 240);
  // SDL_RenderDrawLine(renderer, 340, 240, 320, 200);


  SDL_RenderPresent(renderer);
}
