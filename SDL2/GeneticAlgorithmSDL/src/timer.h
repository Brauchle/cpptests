#ifndef SIMPLE_TIMER_FUNCTIONS__
#define SIMPLE_TIMER_FUNCTIONS__
#include <iostream>

#include <chrono>

namespace timer {
  auto start_time = std::chrono::high_resolution_clock::now();

  void tik(){
    start_time = std::chrono::high_resolution_clock::now();
  }

  void tok(){
    auto stop_time = std::chrono::high_resolution_clock::now();

    auto ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(stop_time - start_time);
    auto s_time = std::chrono::duration_cast<std::chrono::seconds>(stop_time - start_time);
    auto m_time = std::chrono::duration_cast<std::chrono::minutes>(stop_time - start_time);
    auto h_time = std::chrono::duration_cast<std::chrono::hours>(stop_time - start_time);
    std::cerr << " - "  << h_time.count()
              << "h "   << m_time.count() % 60
              << "m "   << s_time.count() % 60
              << "s "   << std::endl;
  }

  void tok(float perc){
    auto stop_time = std::chrono::high_resolution_clock::now();

    auto ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(stop_time - start_time);
    auto s_time = std::chrono::duration_cast<std::chrono::seconds>(stop_time - start_time);
    auto m_time = std::chrono::duration_cast<std::chrono::minutes>(stop_time - start_time);
    auto h_time = std::chrono::duration_cast<std::chrono::hours>(stop_time - start_time);
    std::cerr << " - "  << h_time.count()
              << "h "   << m_time.count() % 60
              << "m "   << s_time.count() % 60
              << "s ";

    ms_time = std::chrono::duration_cast<std::chrono::milliseconds>(stop_time - start_time);
    s_time = std::chrono::duration_cast<std::chrono::seconds>( (stop_time - start_time) * (1.0 / perc) );
    m_time = std::chrono::duration_cast<std::chrono::minutes>( (stop_time - start_time) *(1.0 / perc) );
    h_time = std::chrono::duration_cast<std::chrono::hours>( (stop_time - start_time) * (1.0 / perc) );
    std::cerr << "("  << h_time.count()
              << "h " << m_time.count() % 60
              << "m " << s_time.count() % 60
              << "s)" << std::endl;
  }
}

#endif
