#include "genalg.h"


float rate(Individuum& a){
  // distance between the points is a meassure for the AWGN Channel
  // Amplitude maximum is 1 that means the maximum distance is 2
  // Minimum distance? Average Distance? Not sure
  int genes = a.genes.size();
  double min_dist = 2;
  for (size_t i = 0; i < genes - 1; i++) {
    for (size_t j = i+1; j < genes; j++) {
      double dist = std::sqrt( std::pow(a.genes[i].real() - a.genes[j].real(), 2)
                            + std::pow(a.genes[i].imag() - a.genes[j].imag(), 2) );
      if(dist < min_dist){
        min_dist = dist;
      }
    }
  }

  return min_dist;
}

void init(Population& pop, int genes) {
  std::random_device rd;  //Will be used to obtain a seed for the random number engine
  std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
  std::uniform_real_distribution<float> dis(0.0, 1.0);

  for (Individuum& e : pop){
    e.genes.resize(genes);
    for (size_t i = 0; i < genes; i++) {
      //rx
      float r_amp = dis(gen);
      float r_pha = dis(gen);
      e.genes[i] = r_amp * std::exp( cfloat(0, 2*M_PI*r_pha) );
      // //tx
      // float r_amp = dis(gen);
      // float r_pha = dis(gen);
      // e.tx[i] = r_amp * std::exp( cfloat(0, 2*M_PI*r_pha) );
    }
  }
}

void selection(Population& pop){
  #pragma omp parallel for
  for (Individuum& e:pop) {
    e.fitness = rate(e);
  }
  // puts sorts the vector, smallest fitness elements at the beginning
  std::sort(pop.begin(), pop.end(), [](Individuum& a, Individuum& b){
    return a.fitness < b.fitness;
  });
}

void reproduce(Population& pop, int n){
  // start_of_parents
  int s_parents = n;
  int n_parents = pop.size() - n;

  // genes
  int genes = pop[0].genes.size();

  std::random_device rd;  //Will be used to obtain a seed for the random number engine
  std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
  std::uniform_int_distribution<> dis(0, n_parents - 1);

  #pragma omp parallel for
  for (size_t i = 0; i < s_parents; i++) {
    int p1 = dis(gen);
    int p2 = dis(gen);
    while(p1 == p2){
      p2 = dis(gen);
    }

    // Reproduction split at half of the genes (genes are symbols in our case)
    for (size_t j = 0; j < genes; j++) {
      if(i < genes/2 ){
        pop[i].genes[j] = pop[s_parents + p1].genes[j];
      } else {
        pop[i].genes[j] = pop[s_parents + p2].genes[j];
      }
    }
  }
}

void mutate(Population& pop, int n, float chance){
  int genes = pop[0].genes.size();

  std::random_device rd;  //Will be used to obtain a seed for the random number engine
  std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
  std::uniform_real_distribution<float> dis_g(0.0, 1.0);
  std::uniform_int_distribution<> dis_m(0, genes - 1);

  #pragma omp parallel for
  for (size_t i = 0; i < n; i++) {
    if( chance > dis_g(gen) ) {
      int gene = dis_m(gen);

      float r_amp = dis_g(gen);
      float r_pha = dis_g(gen);
      pop[i].genes[gene] = r_amp * std::exp( cfloat(0, 2*M_PI*r_pha) );
    }
  }
}

void store_generation(std::ofstream& out, Population& pop){
  for (Individuum& e:pop) {
    for (cfloat& cf:e.genes) {
      out.write(reinterpret_cast<char *>(&cf), sizeof cf);
    }
  }
}
