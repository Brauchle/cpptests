#include "sdlwrapper.h"


namespace SDL {
  SDL_Window   *WIN = nullptr;
  SDL_Renderer *REN = nullptr;
}

bool sdl_init() {
  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    return false;
  }

  SDL::WIN = SDL_CreateWindow("Hello World!", 100, 100, SDL::SCREEN_WIDTH,
                              SDL::SCREEN_HEIGHT, SDL_WINDOW_SHOWN);

  if (SDL::WIN == nullptr) {
    SDL_Quit();
    return false;
  }

  SDL::REN = SDL_CreateRenderer(
      SDL::WIN, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (SDL::REN == nullptr) {
    SDL_DestroyWindow(SDL::WIN);
    SDL_Quit();
    return false;
  }

  return true;
}

bool sdl_exit() {
  SDL_DestroyRenderer(SDL::REN);
  SDL_DestroyWindow(SDL::WIN);

  SDL_Quit();
  return true;
}
